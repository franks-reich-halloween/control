"""Tests the service."""
import json

from falcon import testing
import pytest

from control import _app, _create_puppets


class SchedulerForTest(object):
    def __init__(self):
        self.was_scheduled = False

    def setup_pin(self, number):
        pass

    def schedule_event(self, event):
        self.was_scheduled = True


PUPPETS = _create_puppets(SchedulerForTest())


@pytest.fixture
def client():
    """Create falcon test client."""
    test_client = testing.TestClient(_app(SchedulerForTest()))
    return test_client


@pytest.fixture
def client_with_scheduler():
    """Create test client with real scheduler."""
    hardware_scheduler = SchedulerForTest()
    test_client = testing.TestClient(_app(hardware_scheduler))
    return (test_client, hardware_scheduler)


def test_service_should_return_a_puppet_by_identifier(client):
    response = client.simulate_get('/puppet/0')
    actual = json.loads(response.content.decode(encoding='UTF-8'))
    assert actual['identifier'] == 0
    assert actual['name'] == 'Grim Graven Angle'


def test_service_should_return_a_list_of_all_puppets(client):
    response = client.simulate_get('/puppet')
    actual = json.loads(response.content.decode(encoding='UTF-8'))
    for index in range(len(PUPPETS)):
        assert actual[index]['identifier'] == PUPPETS[index].identifier
        assert actual[index]['name'] == PUPPETS[index].name


def test_service_should_return_the_pin_number_of_a_puppet(client):
    response = client.simulate_get('/puppet')
    actual = json.loads(response.content.decode(encoding='UTF-8'))
    for index in range(len(PUPPETS)):
        assert actual[index]['pin_number'] == PUPPETS[index].output.number


class GpioTest(object):
    pass


def test_the_service_should_trigger_the_puppets_if_called(client_with_scheduler):
    response = client_with_scheduler[0].simulate_post('/puppet/0/trigger')
    assert client_with_scheduler[1].was_scheduled is True
