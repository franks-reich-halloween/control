import pytest

from control.hal.MoveStepMotorProcess import MoveStepMotorProcess


class TestCalculateStepAndTimes(object):
    def test_start_3_stop_1_increment_1_pause_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 3, 1, 1, 1)
        expected = [(1, 3), (2, 2), (3, 1), (4, 2)]
        actual = move_step_motor_process._calculate_duty_cycles_and_times()
        assert actual == expected

    def test_start_1_stop_3_increment_1_pause_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 3, 1, 1)
        expected = [(1, 1), (2, 2), (3, 3), (4, 2)]
        actual = move_step_motor_process._calculate_duty_cycles_and_times()
        assert actual == expected

    def test_start_1_stop_2_increment_02_pause_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 2, 0.2, 0.5)
        expected = [
            (0.5, 1.0), (1.0, 1.2), (1.5, 1.4), (2.0, 1.6), (2.5, 1.8),
            (3.0, 2.0), (3.5, 1.8), (4.0, 1.6), (4.5, 1.4), (5.0, 1.2)
        ]
        actual = move_step_motor_process._calculate_duty_cycles_and_times()
        expected_delays = [combination[0] for combination in expected]
        expected_duty_cycles = [combination[1] for combination in expected]
        actual_delays = [combination[0] for combination in actual]
        actual_duty_cycles = [combination[1] for combination in actual]
        assert actual_delays == pytest.approx(expected_delays)
        assert actual_duty_cycles == pytest.approx(expected_duty_cycles)
