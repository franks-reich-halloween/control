import pytest

from control.hal.MoveStepMotorProcess import MoveStepMotorProcess


class TestCalculateStepsDecrementing(object):
    def test_calculate_steps_from_2_to_1_with_increment_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 2, 1, 1, 1)
        expected = [2, 1]
        actual = move_step_motor_process._calculate_steps()
        assert actual == expected

    def test_calculate_steps_from_2_to_1_with_increment_02(self):
        move_step_motor_process = MoveStepMotorProcess(10, 2, 1, 0.2, 1)
        expected = [2.0, 1.8, 1.6, 1.4, 1.2, 1.0, 1.2, 1.4, 1.6, 1.8]
        actual = move_step_motor_process._calculate_steps()
        assert actual == pytest.approx(expected)


class TestCalculateStepsIncrementing(object):
    def test_calculate_steps_from_1_to_2_with_increment_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 2, 1, 1)
        expected = [1, 2]
        actual = move_step_motor_process._calculate_steps()
        assert actual == expected

    def test_calculate_steps_from_1_to_3_with_increment_1(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 3, 1, 1)
        expected = [1, 2, 3, 2]
        actual = move_step_motor_process._calculate_steps()
        assert actual == expected

    def test_calculate_steps_from_1_to_2_with_increment_05(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 2, 0.5, 1)
        expected = [1.0, 1.5, 2.0, 1.5]
        actual = move_step_motor_process._calculate_steps()
        assert actual == pytest.approx(expected)

    def test_calculate_steps_from_1_to_3_with_increment_05(self):
        move_step_motor_process = MoveStepMotorProcess(10, 1, 3, 0.5, 1)
        expected = [1.0, 1.5, 2.0, 2.5, 3.0, 2.5, 2.0, 1.5]
        actual = move_step_motor_process._calculate_steps()
        assert actual == pytest.approx(expected)

    def test_calculate_steps_from_2_to_3_with_increment_03(self):
        move_step_motor_process = MoveStepMotorProcess(10, 2, 3, 0.3, 1)
        expected = [2.0, 2.3, 2.6, 2.9, 2.6, 2.3]
        actual = move_step_motor_process._calculate_steps()
        assert actual == pytest.approx(expected)

    def test_calculate_steps_from_2_to_4_with_increment_01(self):
        move_step_motor_process = MoveStepMotorProcess(10, 2, 3, 0.1, 1)
        expected = [
            2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9,
            3.0, 2.9, 2.8, 2.7, 2.6, 2.5, 2.4, 2.3, 2.2, 2.1
        ]
        actual = move_step_motor_process._calculate_steps()
        assert actual == pytest.approx(expected)
