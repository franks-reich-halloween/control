"""Store image puppet mapping"""
import mimetypes
from PIL import Image


puppet_image_path_prefix = './images/puppets/'


class PuppetImageCollection(object):
    def __init__(self, collection, storage):
        self.collection = collection
        self.CHUNK_SIZE = 4096
        self.storage = storage

    def image_name_by_puppet_identifier(self, identifier):
        """Return image name for puppet identifier"""
        image_name = self.collection[str(identifier)]
        return puppet_image_path_prefix + image_name

    def store_puppet_image(self, identifier, stream, content_type):
        """Store image for puppet"""
        extension = mimetypes.guess_extension(content_type)
        file_name = str(identifier) + extension
        file_path = puppet_image_path_prefix + '/' + file_name
        image = Image.open(stream)
        image.thumbnail((1024, 1024))
        image.save(file_path)
        self.collection[str(identifier)] = file_name
        self._schedule_update()

    def to_dict(self):
        """Return dict for json conversion"""
        return self.collection

    def _schedule_update(self):
        data = self.to_dict()
        self.storage.store_image_map(data)
