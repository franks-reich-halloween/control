"""Classes representing the model for step motor controllers."""
import logging


class StepMotorController(object):
    """Represents a step motor for movement of objects"""

    def __init__(self, name, output, output_pin_mapping, parameters, identifier=-1):
        self.name = name
        self.output = output
        self.parameters = parameters
        self.output_pin_mapping = output_pin_mapping
        self.identifier = identifier

    @property
    def pin(self):
        """Return output pin according to output <-> pin mapping"""
        return self.output_pin_mapping[self.output]

    def to_dict(self, include_running=False, running_identifiers=None):
        if include_running:
            running = self.identifier in running_identifiers
            return {
                "identifier": self.identifier,
                "name": self.name,
                "output": self.output,
                "parameters": self.parameters.to_dict(),
                "running": running
            }
        else:
            return {
                "identifier": self.identifier,
                "name": self.name,
                "output": self.output,
                "parameters": self.parameters.to_dict()
            }


class StepMotorParameters(object):
    """Parameters controlling the step motor"""

    def __init__(self, frequency, duty_cycle_start, duty_cycle_stop, increment, pause):
        self.frequency = float(frequency)
        self.duty_cycle_start = float(duty_cycle_start)
        self.duty_cycle_stop = float(duty_cycle_stop)
        self.increment = float(increment)
        self.pause = float(pause)

    def to_dict(self):
        """Convert to dictionary for json conversion"""
        return {
            "frequency": self.frequency,
            "duty_cycle_start": self.duty_cycle_start,
            "duty_cycle_stop": self.duty_cycle_stop,
            "increment": self.increment,
            "pause": self.pause
        }


class StepMotorControllerCollection(object):
    """A collection of step motor controller"""

    def __init__(self, step_motor_controllers, storage, hardware_scheduler,
                 output_pin_mapping):
        self.controllers = step_motor_controllers
        self.storage = storage
        self.hardware_scheduler = hardware_scheduler
        self.output_pin_mapping = output_pin_mapping

    def controller_by_identifier(self, identifier):
        return self.controllers[str(identifier)]

    def has_identifier(self, identifier):
        return str(identifier) in self.controllers

    def update(self, step_motor_controller):
        """Update a step motor controller"""
        logging.info("Updating a step motor controller")
        step_motor_controller.identifier = str(step_motor_controller.identifier)
        self.controllers[step_motor_controller.identifier] = step_motor_controller
        self._schedule_update()

    def _schedule_update(self):
        data = self.to_dict()
        self.storage.store_step_motor_controller(data)

    def to_dict(self):
        """Convert to dictionary for json conversion"""
        return dict((key, step_motor_controller.to_dict()) for (key, step_motor_controller)
                    in self.controllers.items())

    def to_list(self):
        """Convert to list for json conversion"""
        return [step_motor_controller.to_dict(
                    True, self.hardware_scheduler.running_step_motor_controller)
                for (key, step_motor_controller) in self.controllers.items()]
