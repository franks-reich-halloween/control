"""Output pin mapping"""


class OutputPin(object):
    """Hardware output for rapberry PI"""

    def __init__(self, output, pin, is_pwm=False):
        self.output = output
        self.pin = pin
        self.is_pwm=is_pwm

    def to_dict(self):
        """Return dict for json conversion"""
        return {
            'output': self.output,
            'pin': self.pin
        }


outputs = [
    OutputPin(1, 17),
    OutputPin(2, 27),
    OutputPin(3, 22),
    OutputPin(4, 5),
    OutputPin(5, 6),
    OutputPin(6, 13),
    OutputPin(7, 19),
    OutputPin(8, 26),
    OutputPin(9, 18),
    OutputPin(10, 23),
    OutputPin(11, 12, True)
]


def setup_output_pins(hardware_scheduler):
    hardware_scheduler.setup_all_pins(outputs)


class OutputPinCollection(object):
    """Collection of output pins"""

    def __init__(self):
        self.outputs = outputs

    def to_dict(self):
        """Return dict for json conversion"""
        return [output.to_dict() for output in outputs]


def create_output_pin_mapping(pwm=False):
    output_pin_mapping = {}
    if not pwm:
        for output in [output for output in outputs if not output.is_pwm]:
            output_pin_mapping[output.output] = output.pin
    else:
        for output in [output for output in outputs if output.is_pwm]:
            output_pin_mapping[output.output] = output.pin
    return output_pin_mapping
