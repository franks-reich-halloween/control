"""Classes representing the model for puppets."""
import logging


class Puppet(object):
    """Represents a halloween puppet that can be controlled."""

    def __init__(self, name, output, output_pin_mapping, identifier=-1):
        self.identifier = str(identifier)
        self.name = name
        self.output = output
        self.output_pin_mapping = output_pin_mapping

    @property
    def pin(self):
        """Return output pin according to output <-> pin mapping"""
        return self.output_pin_mapping[self.output]

    def trigger(self):
        """Trigger the puppet connected to the output pin."""
        self.output.trigger()

    def to_dict(self):
        """Convert to dict for json conversion"""
        return {
            'identifier': self.identifier,
            'name': self.name,
            'output': self.output
        }


class OutputAlreadyInUseError(Exception):
    """Only one puppet can use a given output exception"""
    pass


class PuppetIsStillInUseError(Exception):
    """The Puppet is still in use and can't be deleted"""

    def __init__(self, puppet_used_by):
        self.puppet_used_by = puppet_used_by


class PuppetCollection(object):
    """A collection of puppets."""

    def __init__(self, puppets, choreographies, storage):
        self.puppets = puppets
        self.choreographies = choreographies
        self.storage = storage
        if len(puppets) == 0:
            self.next_key = 0
        else:
            self.next_key = max(map(lambda k: int(k), puppets)) + 1

    def puppet_by_identifier(self, identifier):
        """Return puppet with the identifier."""
        return self.puppets[str(identifier)]

    def has_identifier(self, identifier):
        """Returns true if the identifier exists"""
        return str(identifier) in self.puppets

    def insert(self, puppet):
        """Insert a puppet, update the identifier and schedule filesystem update"""
        logging.info("Inserting new puppet")
        if self._is_output_in_use(puppet.output):
            raise OutputAlreadyInUseError()
        index = str(self.next_key)
        self.next_key += 1
        puppet.identifier = index
        self.puppets[puppet.identifier] = puppet
        self._schedule_update()
        return puppet

    def update(self, puppet):
        """Update an existing puppet and schedule filesystem update"""
        logging.info("Updating new puppet")
        puppet.identifier = str(puppet.identifier)
        if self._is_output_in_use_by_other_puppet(puppet.output, puppet.identifier):
            raise OutputAlreadyInUseError()
        self.puppets[puppet.identifier] = puppet
        self._schedule_update()

    def delete(self, puppet_identifier):
        """Delete an existing puppet if the puppet is not used in a choreography"""
        puppet_used_by = self.choreographies.puppet_used_by(puppet_identifier)
        if len(puppet_used_by) > 0:
            raise PuppetIsStillInUseError(puppet_used_by)
        del self.puppets[str(puppet_identifier)]
        self._schedule_update()

    def to_dict(self):
        """Convert to dict for json conversion"""
        return dict((key, puppet.to_dict()) for (key, puppet) in self.puppets.items())

    def to_list(self):
        """Convert to list for json conversion"""
        return [puppet.to_dict() for key, puppet in self.puppets.items()]

    def _is_output_in_use_by_other_puppet(self, output, identifier):
        puppets_with_output =\
            [puppet for key, puppet in self.puppets.items()
             if puppet.output == output and puppet.identifier != str(identifier)]
        return len(puppets_with_output) > 0

    def _is_output_in_use(self, output):
        puppets_with_output =\
            [puppet for key, puppet in self.puppets.items() if puppet.output == output]
        return len(puppets_with_output) > 0

    def _schedule_update(self):
        data = self.to_dict()
        self.storage.store_puppets(data)
