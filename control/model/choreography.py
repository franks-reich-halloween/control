"""Implements the model for choreographies"""
import logging
from enum import Enum


class Choreography(object):
    """Represents a choreography of halloween puppets and pauses"""

    def __init__(self, name, events, identifier=-1):
        self.identifier = identifier
        self.name = name
        self.events = events

    def to_dict(self, include_running=False, running_choreography_identifier=None):
        """Convert to dictionary for json conversion"""
        if include_running:
            return {
                "name": self.name,
                "identifier": self.identifier,
                "events": list(map(lambda event: event.to_dict(), self.events)),
                "running": self.identifier == running_choreography_identifier
            }
        else:
            return {
                "name": self.name,
                "identifier": self.identifier,
                "events": list(map(lambda event: event.to_dict(), self.events))
            }



class ChoreographyEventType(Enum):
    PAUSE = 1
    PUPPET = 2


class ChoreographyPuppetEvent(object):
    """Represents a trigger puppet event in a choreography"""

    def __init__(self, name, identifier, output=None):
        self.name = name
        self.identifier = str(identifier)
        self.event_type = ChoreographyEventType.PUPPET
        self.output = output

    def to_dict(self):
        """Convert to dictionary for json conversion"""
        return {
            "type": "Puppet",
            "name": self.name,
            "identifier": self.identifier
        }


class ChoreographyPauseEvent(object):
    """Represents a pause event in a choreography"""

    def __init__(self, length):
        self.length = length
        self.event_type = ChoreographyEventType.PAUSE

    def to_dict(self):
        """Convert to dictionary for json conversion"""
        return {
            "type": "Pause",
            "length": self.length
        }


class ChoreographyCollection(object):
    """A collection of choreographies."""

    def __init__(self, choreographies, storage, hardware_scheduler):
        self.choreographies = choreographies
        self.storage = storage
        self.hardware_scheduler = hardware_scheduler
        if len(choreographies) == 0:
            self.next_key = 0
        else:
            self.next_key = max(map(lambda k: int(k), choreographies)) + 1

    def choreography_by_identifier(self, identifier):
        """Return choreography with the identifier"""
        return self.choreographies[str(identifier)]

    def has_identifier(self, identifier):
        """Returns true if the identifier exists"""
        return str(identifier) in self.choreographies

    def insert(self, choreography):
        """Insert a choreography, update the identifier and schedule filesystem update"""
        logging.info("Inserting new choreography")
        index = str(self.next_key)
        self.next_key += 1
        choreography.identifier = index
        self.choreographies[index] = choreography
        self._schedule_update()
        return choreography

    def update(self, choreography):
        """Update the choreography with the identifier"""
        logging.info("Updating new choreography")
        choreography.identifier = str(choreography.identifier)
        self.choreographies[choreography.identifier] = choreography
        self._schedule_update()

    def delete(self, identifier):
        """Delete the choreography with the identifier"""
        logging.info("Deleting choreography: " + str(identifier))
        del self.choreographies[str(identifier)]
        self._schedule_update()

    def puppet_used_by(self, puppet_identifier):
        """Returns a list of choreographies that use the given puppet"""
        return [choreography.identifier for key, choreography in self.choreographies.items()
                if len([event for event in choreography.events
                        if event.event_type == ChoreographyEventType.PUPPET and
                        str(event.identifier) == str(puppet_identifier)]) > 0]

    def to_dict(self):
        """Convert to dictionary for json conversion"""
        return \
            dict((key, choreography.to_dict()) for (key, choreography)
                 in self.choreographies.items())

    def to_list(self):
        """Convert to list for json conversion"""
        return [choreography.to_dict(True, self.hardware_scheduler.repeating_choreography_identifier)
                for key, choreography in self.choreographies.items()]

    def _schedule_update(self):
        data = self.to_dict()
        self.storage.store_choreographies(data)
