"""Process to repeatedly execute a list of events"""
import logging
import sched
import multiprocessing

from .ScheduleTools import schedule_events


def _repeated_scheduler(events, stop_event):
    logging.basicConfig(level=logging.INFO)
    running = True
    while running:
        logging.info('Looping repeating choreography process')
        scheduler = sched.scheduler()
        logging.info('Scheduling events')
        schedule_events(scheduler, events)
        scheduler.run()
        running = not stop_event.is_set()
    logging.info('Ending repeating choreography process')


class ScheduleRepeatedChoreographyProcess(object):
    """Starts a thread that allows to repeatedly schedule events"""

    def __init__(self, events):
        self.stop_event = multiprocessing.Event()
        self.process = multiprocessing.Process(
            target=_repeated_scheduler, args=(list(events), self.stop_event))

    def start(self):
        """Start the process"""
        logging.info('Starting repeating choreography process')
        self.process.start()

    def stop(self):
        """Stop the process"""
        logging.info('Stopping repeating choreography process')
        self.stop_event.set()
