"""Hardware Abstraction Layer"""
import multiprocessing as mp

from .HardwareSchedulerClient import HardwareSchedulerClient
from .ControlMessage import Command, ControlMessage
from .SchedulerProcess import scheduler_process


class HardwareScheduler(object):
    """Schedules and executes hardware events."""

    def __init__(self):
        self.process = None
        self.running = False
        self.manager = mp.Manager()
        self.message_queue = self.manager.Queue()
        self._create_process()

    def start(self):
        """Starts the process for the scheduler."""
        self._start_process()
        self.running = True

    def _create_process(self):
        self.process = mp.Process(target=scheduler_process, args=(self.message_queue,))

    def _start_process(self):
        self.process.start()

    def stop(self):
        """Stop the process for the scheduler."""
        message = ControlMessage(Command.SHUTDOWN)
        self.message_queue.put(message)
        self.process.join()

    def client(self):
        return HardwareSchedulerClient(self.message_queue)
