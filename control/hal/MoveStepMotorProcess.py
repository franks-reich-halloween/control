"""Process to move a step motor"""
import logging
import sched
import multiprocessing
import math

import RPi.GPIO as GPIO


def _change_duty_cycle(pwm_pin, duty_cycle):
    pwm_pin.ChangeDutyCycle(duty_cycle)


def _schedule_pwm_events(events, pwm_pin, scheduler):
    for event in events:
        scheduler.enter(event[0], 1, _change_duty_cycle, argument=(pwm_pin, event[1]))


def _run_pwm_modulation(events, stop_event, pin_number, repeat=True):
    logging.basicConfig(level=logging.INFO)
    pwm_pin = GPIO.PWM(pin_number, 50)
    pwm_pin.start(0)
    running = True
    while running:
        scheduler = sched.scheduler()
        _schedule_pwm_events(events, pwm_pin, scheduler)
        scheduler.run()
        running = not stop_event.is_set() and repeat
    logging.info('Ending pwm process')


class MoveStepMotorProcess(object):
    """Move a step motor using a PWM pin"""

    def __init__(
            self, frequency, duty_cycle_start, duty_cycle_stop,
            increment, pause, pin_number, repeat=True):
        self.frequency = frequency
        self.duty_cycle_start = duty_cycle_start
        self.duty_cycle_stop = duty_cycle_stop
        self.increment = increment
        self.pause = pause
        self.stop_event = multiprocessing.Event()
        self.pin_number = pin_number
        self.process = multiprocessing.Process(
            target=_run_pwm_modulation,
            args=(self._calculate_duty_cycles_and_times(), self.stop_event,
                  self.pin_number, repeat))

    def start(self):
        """Start the process"""
        logging.info('Starting pwm process')
        self.process.start()

    def stop(self):
        """Stop the process"""
        logging.info('Stopping pwm process')
        self.stop_event.set()

    def _calculate_duty_cycles_and_times(self):
        duty_cycles = self._calculate_steps()
        delays = self._calculate_times(len(duty_cycles))
        return list(zip(delays, duty_cycles))

    def _calculate_times(self, length):
        steps = range(1, length + 1)
        return [step * self.pause for step in steps]

    def _calculate_steps(self):
        if self.duty_cycle_start < self.duty_cycle_stop:
            return self._calculate_steps_incrementing()
        else:
            return self._calculate_steps_decrementing()

    def _calculate_steps_decrementing(self):
        difference = self.duty_cycle_start - self.duty_cycle_stop
        number_of_steps = int(math.floor(difference / self.increment))
        result = []
        value = self.duty_cycle_start
        for step in range(number_of_steps + 1, 1, -1):
            result.append(value)
            value -= self.increment
        for step in range(1, number_of_steps + 1):
            result.append(value)
            value += self.increment
        return result

    def _calculate_steps_incrementing(self):
        difference = self.duty_cycle_stop - self.duty_cycle_start
        number_of_steps = int(math.floor(difference / self.increment))
        result = []
        value = self.duty_cycle_start
        for step in range(1, number_of_steps + 1):
            result.append(value)
            value += self.increment
        for step in range(number_of_steps + 1, 1, -1):
            result.append(value)
            value -= self.increment
        return result
