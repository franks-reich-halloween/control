"""A command for the hardware scheduler"""
from enum import Enum


class Command(Enum):
    SCHEDULE_EVENTS = 1
    SHUTDOWN = 2
    SETUP_PINS = 3
    START_REPEATED_EVENTS = 4
    STOP_REPEATED_EVENTS = 5
    START_STEP_MOTOR_CONTROLLER = 6
    STOP_STEP_MOTOR_CONTROLLER = 7
    TRIGGER_STEP_MOTOR_CONTROLLER = 8


class ControlMessage(object):
    """A message exchanged by the processes."""

    def __init__(self, command, events=None):
        self.command = command
        self.events = events
