import logging
import sched
import RPi.GPIO as GPIO

from .Event import EventType


def schedule_events(scheduler, events):
    start_time = 0
    for event in events:
        if event.event_type == EventType.PUPPET:
            _schedule_trigger_event(scheduler, event.data.pin, start_time)
            logging.info('Schedule GPIO.output for pin {} at time {}'
                         .format(event.data.pin, start_time))
        elif event.event_type == EventType.PAUSE:
            start_time += event.data.length
    return start_time


def schedule_and_execute_events(events):
    scheduler = sched.scheduler()
    schedule_events(scheduler, events)
    scheduler.run()
    return True


def _schedule_trigger_event(scheduler, pin, start_time):
    scheduler.enter(start_time, 1, GPIO.output, argument=(pin, GPIO.HIGH))
    scheduler.enter(start_time + 0.5, 1, GPIO.output, argument=(pin, GPIO.LOW))
