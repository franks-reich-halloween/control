"""An event for the hardware scheduler"""
from enum import Enum


class EventType(Enum):
    PAUSE = 1
    PUPPET = 2


class Event(object):
    """An event to be scheduled."""

    def __init__(self, event_type, data):
        self.event_type = event_type
        self.data = data
