"""Client for the hardware scheduler"""
import logging

from .ControlMessage import Command, ControlMessage
from control.model import ChoreographyEventType
from .Event import EventType, Event


def _create_setup_pin_event(event):
    return Event(EventType.PUPPET, event)


def _create_choreography_events(event):
    if event.event_type == ChoreographyEventType.PAUSE:
        return Event(EventType.PAUSE, event)
    else:
        return Event(EventType.PUPPET, event)


class HardwareSchedulerClient(object):
    def __init__(self, message_queue):
        self.message_queue = message_queue
        self.repeating_choreography_identifier = None
        self.running_step_motor_controller = {}

    def setup_all_pins(self, output_pins):
        """Schedule setup for all pins"""
        events = map(_create_setup_pin_event, output_pins)
        message = ControlMessage(Command.SETUP_PINS, events)
        self.message_queue.put(message)

    def trigger_puppet(self, puppet):
        """Schedule triggering of a puppet"""
        output_number = puppet.output
        logging.info('Scheduling trigger event for output {}'.format(output_number))
        message = ControlMessage(Command.SCHEDULE_EVENTS, [Event(EventType.PUPPET, puppet)])
        self.message_queue.put(message)

    def trigger_choreography_events(self, events):
        """Schedule the events of a choreography"""
        logging.info('Triggering choreography events')
        events = map(_create_choreography_events, events)
        message = ControlMessage(Command.SCHEDULE_EVENTS, events)
        self.message_queue.put(message)

    def trigger_step_motor_controller(self, controller):
        """Schedule the execution of the step motor controller once"""
        logging.info('Start step motor controller once')
        if controller.identifier not in self.running_step_motor_controller:
            message = ControlMessage(Command.TRIGGER_STEP_MOTOR_CONTROLLER, controller)
            self.message_queue.put(message)

    def start_repeating_step_motor_controller(self, controller):
        """Schedule the execution of the step motor controller"""
        logging.info('Start repeating step motor controller')
        if controller.identifier not in self.running_step_motor_controller:
            self.running_step_motor_controller[controller.identifier] = True
            message = ControlMessage(Command.START_STEP_MOTOR_CONTROLLER, controller)
            self.message_queue.put(message)

    def stop_repeating_step_motor_controller(self, controller):
        """Stop the scheduled repeating step motor"""
        logging.info('Stop repeating step motor controller')
        if controller.identifier in self.running_step_motor_controller:
            del self.running_step_motor_controller[controller.identifier]
            message = ControlMessage(Command.STOP_STEP_MOTOR_CONTROLLER, controller)
            self.message_queue.put(message)

    def start_repeating_choreography(self, events, identifier):
        """Start repeating choreography if no choreography is running"""
        if self.repeating_choreography_identifier is None:
            logging.info('Start repeating choreography {}'.format(identifier))
            self.repeating_choreography_identifier = identifier
            events = map(_create_choreography_events, events)
            message = ControlMessage(Command.START_REPEATED_EVENTS, events)
            self.message_queue.put(message)

    def stop_repeating_choreography(self):
        """Stop repeating choreography if choreography is running"""
        if self.repeating_choreography_identifier is not None:
            logging.info('Stop repeating choreography')
            self.repeating_choreography_identifier = None
            message = ControlMessage(Command.STOP_REPEATED_EVENTS)
            self.message_queue.put(message)
