"""The process running to schedule the events"""
from concurrent.futures import ThreadPoolExecutor
import signal
import RPi.GPIO as GPIO
import logging

from .ControlMessage import Command
from .ScheduleTools import schedule_and_execute_events
from .ScheduleRepeatedChoreographyProcess import ScheduleRepeatedChoreographyProcess
from .MoveStepMotorProcess import MoveStepMotorProcess


def scheduler_process(message_queue):
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    logging.basicConfig(level=logging.INFO)
    GPIO.setmode(GPIO.BCM)
    futures = []
    executor = ThreadPoolExecutor()
    running = True
    repeated_choreography_thread = None
    repeating_step_motor_controller_processes = {}
    while running:
        message = message_queue.get()
        if message.command == Command.SHUTDOWN:
            logging.info('Shutting down scheduler process')
            for future in futures:
                future.result()
            running = False
        elif message.command == Command.SCHEDULE_EVENTS:
            logging.info('Scheduling events')
            future = executor.submit(schedule_and_execute_events, message.events)
            futures.append(future)
        elif message.command == Command.START_REPEATED_EVENTS:
            if repeated_choreography_thread is None:
                logging.info('Starting repeated events')
                repeated_choreography_thread = ScheduleRepeatedChoreographyProcess(
                    message.events)
                repeated_choreography_thread.start()
        elif message.command == Command.STOP_REPEATED_EVENTS:
            if repeated_choreography_thread is not None:
                logging.info('Stopping repeated events')
                repeated_choreography_thread.stop()
                repeated_choreography_thread = None
        elif message.command == Command.SETUP_PINS:
            for event in message.events:
                logging.info('Setting up pin {}'.format(event.data.pin))
                GPIO.setup(event.data.pin, GPIO.OUT)
        elif message.command == Command.START_STEP_MOTOR_CONTROLLER:
            identifier = message.events.identifier
            logging.info('Starting step motor controller for process {}'
                         .format(identifier))
            repeating_step_motor_controller_processes[identifier] = \
                MoveStepMotorProcess(
                    message.events.parameters.frequency,
                    message.events.parameters.duty_cycle_start,
                    message.events.parameters.duty_cycle_stop,
                    message.events.parameters.increment,
                    message.events.parameters.pause,
                    message.events.pin)
            repeating_step_motor_controller_processes[identifier].start()
        elif message.command == Command.STOP_STEP_MOTOR_CONTROLLER:
            identifier = message.events.identifier
            logging.info('Stopping step motor controller for process {}'
                         .format(identifier))
            if identifier in repeating_step_motor_controller_processes:
                repeating_step_motor_controller_processes[identifier].stop()
                del repeating_step_motor_controller_processes[identifier]
        elif message.command == Command.TRIGGER_STEP_MOTOR_CONTROLLER:
            identifier = message.events.identifier
            logging.info('Triggering step motor controller for process {} once'
                         .format(identifier))
            process = \
                MoveStepMotorProcess(
                    message.events.parameters.frequency,
                    message.events.parameters.duty_cycle_start,
                    message.events.parameters.duty_cycle_stop,
                    message.events.parameters.increment,
                    message.events.parameters.pause,
                    message.events.pin,
                    False)
            process.start()
    if repeated_choreography_thread is not None:
        repeated_choreography_thread.stop()
    for process in repeating_step_motor_controller_processes:
        process.stop()
