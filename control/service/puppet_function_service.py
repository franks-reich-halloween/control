import simplejson as json
import logging


class PuppetFunctionService(object):
    """Execute functions of puppets."""

    def __init__(self, collection, hardware_scheduler):
        self.collection = collection
        self.hardware_scheduler = hardware_scheduler

    def on_post(self, _, response, puppet_identifier, puppet_function):
        """Handle post request."""
        logging.info('Received POST request for function execution of puppet.')
        if puppet_function == 'trigger':
            puppet = self.collection.puppet_by_identifier(int(puppet_identifier))
            self.hardware_scheduler.trigger_puppet(puppet)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
