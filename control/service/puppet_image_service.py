import os
import mimetypes
import falcon
import simplejson as json


class PuppetImageService(object):
    """Service images of puppets"""

    def __init__(self, collection):
        self.collection = collection

    def on_post(self, request, response, puppet_identifier):
        identifier = int(puppet_identifier)
        self.collection.store_puppet_image(identifier, request.stream, request.content_type)
        response.content_type = 'application/json_conversion'
        response.body = json.dumps({'success': True})

    def on_get(self, _, response, puppet_identifier):
        try:
            image_path = self.collection.image_name_by_puppet_identifier(int(puppet_identifier))
            stream, stream_length = self._open_image(image_path)
            response.content_type = mimetypes.guess_type(image_path)[0]
            response.stream = stream
            response.stream_len = stream_length
        except IndexError:
            response.status = falcon.HTTP_404

    def _open_image(self, image_path):
        stream = open(image_path, 'rb')
        stream_length = os.path.getsize(image_path)
        return stream, stream_length
