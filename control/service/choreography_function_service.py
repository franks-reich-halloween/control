"""Handles functions for choreographies"""
import logging
import simplejson as json
from control.model import ChoreographyEventType


def _add_pin_number_to_event(collection, event):
    if event.event_type == ChoreographyEventType.PUPPET:
        puppet = collection.puppet_by_identifier(event.identifier)
        event.pin = puppet.pin
        return event
    else:
        return event


class ChoreographyFunctionService(object):
    """Handles functions for choreographies"""

    def __init__(self, puppet_collection, choreography_collection, hardware_scheduler):
        self.puppet_collection = puppet_collection
        self.choreography_collection = choreography_collection
        self.hardware_scheduler = hardware_scheduler

    def on_post(self, _, response, choreography_identifier, choreography_function):
        """Trigger the events of the choreography"""
        logging.info('Received POST request for function: ' + choreography_function)
        if choreography_function == 'trigger':
            choreography = self.choreography_collection.choreography_by_identifier(
                choreography_identifier)
            events = [_add_pin_number_to_event(self.puppet_collection, event) for event in choreography.events]
            self.hardware_scheduler.trigger_choreography_events(events)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        if choreography_function == 'start_repeat':
            choreography = self.choreography_collection.choreography_by_identifier(
                choreography_identifier)
            events = [_add_pin_number_to_event(self.puppet_collection, event) for event in choreography.events]
            self.hardware_scheduler.start_repeating_choreography(events, choreography_identifier)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        if choreography_function == 'stop_repeat':
            self.hardware_scheduler.stop_repeating_choreography()
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
