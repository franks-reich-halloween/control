import simplejson as json


class StepMotorControllerCollectionService(object):
    """Collection of Step Motor Controller"""

    def __init__(self, collection):
        self.collection = collection

    def on_get(self, _, response):
        """Return list of all step motor controllers"""
        response.body = json.dumps(self.collection.to_list())
        response.content_type = 'application/json'
        response.location = '/step_motor_controller'
