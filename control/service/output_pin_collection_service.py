import simplejson as json
from control.model import OutputPinCollection


class OutputPinCollectionService(object):
    """Collection of output pins"""

    def __init__(self):
        self.collection = OutputPinCollection()

    def on_get(self, _, response):
        """Return list of all output pins"""
        response.body = json.dumps(self.collection.to_dict())
        response.content_type = 'application/json'
        response.location = '/puppet'
