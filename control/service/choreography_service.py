import simplejson as json
import falcon

from control.json_conversion import json_object_to_choreography


class ChoreographyService(object):
    """Service for a single choreography"""

    def __init__(self, collection):
        self.collection = collection

    def on_get(self, _, response, choreography_identifier):
        """Return choreography with the given identifier"""
        index = int(choreography_identifier)
        response.body = json.dumps(
            self.collection.choreography_by_identifier(index).to_dict())
        response.content_type = 'application/json'
        response.location = '/choreography'

    def on_post(self, request, response, choreography_identifier):
        """Update choreography with identifier and return status code 200"""
        index = choreography_identifier
        json_object = json.load(request.bounded_stream)
        choreography = json_object_to_choreography(json_object)
        if index == choreography.identifier and self.collection.has_identifier(index):
            self.collection.update(choreography)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        else:
            response.status = falcon.HTTP_400

    def on_delete(self, _, response, choreography_identifier):
        """Delete the choreography with identifier and return status code 200"""
        self.collection.delete(choreography_identifier)
        response.content_type = 'application/json_conversion'
        response.body = json.dumps({'success': True})
