import simplejson as json

from control.json_conversion import json_object_to_choreography


class ChoreographyCollectionService(object):
    """Collection of Choreographies"""

    def __init__(self, collection):
        self.collection = collection

    def on_post(self, request, response):
        """Add a new Choreography to the collection"""
        json_object = json.load(request.bounded_stream)
        choreography = json_object_to_choreography(json_object)
        choreography = self.collection.insert(choreography)
        response.body = json.dumps(choreography.to_dict())
        response.content_type = 'application/json'
        response.location = '/choreography/' + str(choreography.identifier)

    def on_get(self, _, response):
        """Return list of all choreographies"""
        response.body = json.dumps(self.collection.to_list())
        response.content_type = 'application/json'
        response.location = '/choreography'
