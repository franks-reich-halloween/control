import falcon
import simplejson as json

from control.json_conversion import json_object_to_step_motor_controller


class StepMotorControllerService(object):
    def __init__(self, collection):
        self.collection = collection

    def on_get(self, _, response, step_motor_controller_identifier):
        """Return the step motor controller"""
        step_motor_controller = self.collection.controller_by_identifier(
            step_motor_controller_identifier)
        response.body = json.dumps(step_motor_controller.to_dict())
        response.content_type = 'application/json_conversion'
        response.location = '/puppet/' + step_motor_controller_identifier

    def on_post(self, request, response, step_motor_controller_identifier):
        """Update step motor controller with the given identifier"""
        index = step_motor_controller_identifier
        json_object = json.load(request.bounded_stream)
        step_motor_controller = json_object_to_step_motor_controller(json_object)
        if index == step_motor_controller.identifier and \
           self.collection.has_identifier(step_motor_controller_identifier):
            self.collection.update(step_motor_controller)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        else:
            response.status = falcon.HTTP_400
