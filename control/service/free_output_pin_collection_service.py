import simplejson as json
from control.model import OutputPinCollection


class FreeOutputPinCollectionService(object):
    """Collection of output pins"""

    def __init__(self, puppet_collection):
        self.collection = OutputPinCollection()
        self.puppet_collection = puppet_collection

    def on_get(self, _, response):
        """Return list of all output pins"""
        puppets = self.puppet_collection.puppets.items()
        used_outputs = [puppet.output for (key, puppet) in puppets]
        outputs = [output for output in self.collection.outputs if not output.is_pwm]
        free_outputs = [output.to_dict() for output in outputs
                        if used_outputs.count(output.output) == 0]
        response.body = json.dumps(free_outputs)
        response.content_type = 'application/json'
        response.location = '/puppet'
