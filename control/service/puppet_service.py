import falcon
import simplejson as json

from control.json_conversion import json_object_to_puppet
from control.model import OutputAlreadyInUseError, PuppetIsStillInUseError


class PuppetService(object):
    """Resource for querying and controlling halloween puppets."""

    def __init__(self, collection):
        self.collection = collection

    def on_get(self, _, response, puppet_identifier):
        """Return puppet by identifier."""
        puppet = self.collection.puppet_by_identifier(int(puppet_identifier))
        response.body = json.dumps(puppet.to_dict())
        response.content_type = 'application/json_conversion'
        response.location = '/puppet/' + puppet_identifier

    def on_post(self, request, response, puppet_identifier):
        """Update puppet with given identifier"""
        index = puppet_identifier
        json_object = json.load(request.bounded_stream)
        puppet = json_object_to_puppet(json_object)
        try:
            if index == puppet.identifier and self.collection.has_identifier(index):
                self.collection.update(puppet)
                response.content_type = 'application/json_conversion'
                response.body = json.dumps({'success': True})
            else:
                response.status = falcon.HTTP_400
        except OutputAlreadyInUseError:
            response.status = falcon.HTTP_422
            response.body = json.dumps({'error': 'Output is already in use'})

    def on_delete(self, request, response, puppet_identifier):
        """Delete puppet with given identifier"""
        try:
            self.collection.delete(puppet_identifier)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        except PuppetIsStillInUseError as error:
            response.status = falcon.HTTP_422
            response.body = json.dumps(
                {
                    'error': 'Puppet is still used in choreographies',
                    'used_by': error.puppet_used_by
                })
