import simplejson as json
import falcon

from control.json_conversion import json_object_to_puppet
from control.model import OutputAlreadyInUseError


class PuppetCollectionService(object):
    """Collection of puppet resources."""

    def __init__(self, collection):
        self.collection = collection

    def on_post(self, request, response):
        """Add a new puppet to the collection"""
        json_object = json.load(request.bounded_stream)
        puppet = json_object_to_puppet(json_object)
        try:
            puppet = self.collection.insert(puppet)
            response.body = json.dumps(puppet.to_dict())
            response.content_type = 'application/json'
            response.location = '/puppet/' + str(puppet.identifier)
        except OutputAlreadyInUseError:
            response.status = falcon.HTTP_400
            response.body = json.dumps({'error': 'Output is already in use'})

    def on_get(self, _, response):
        """Return list of all puppets."""
        response.body = json.dumps(self.collection.to_list())
        response.content_type = 'application/json'
        response.location = '/puppet'
