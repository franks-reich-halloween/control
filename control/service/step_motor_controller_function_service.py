import simplejson as json
import logging


class StepMotorControllerFunctionService(object):
    """Execute functions of step motor controllers"""

    def __init__(self, collection, hardware_scheduler):
        self.collection = collection
        self.hardware_scheduler = hardware_scheduler

    def on_post(self, _, response,
                step_motor_controller_identifier,
                step_motor_controller_function):
        """Handle post request"""
        logging.info('Received POST request for function execution of step motor controller')
        if step_motor_controller_function == 'trigger':
            step_motor_controller = self.collection.controller_by_identifier(
                step_motor_controller_identifier)
            self.hardware_scheduler.trigger_step_motor_controller(step_motor_controller)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        elif step_motor_controller_function == 'start_repeat':
            step_motor_controller = self.collection.controller_by_identifier(
                step_motor_controller_identifier)
            self.hardware_scheduler.start_repeating_step_motor_controller(
                step_motor_controller)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
        elif step_motor_controller_function == 'stop_repeat':
            step_motor_controller = self.collection.controller_by_identifier(
                step_motor_controller_identifier)
            self.hardware_scheduler.stop_repeating_step_motor_controller(
                step_motor_controller)
            response.content_type = 'application/json_conversion'
            response.body = json.dumps({'success': True})
