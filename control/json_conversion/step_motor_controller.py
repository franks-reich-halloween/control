"""Step motor controller related json conversions"""
from control.model import StepMotorController, StepMotorParameters, \
                          StepMotorControllerCollection, create_output_pin_mapping


def json_object_to_step_motor_parameters(json_object):
    """Convert dictionary to step motor controller parameters"""
    frequency = float(json_object['frequency'])
    duty_cycle_start = float(json_object['duty_cycle_start'])
    duty_cycle_stop = float(json_object['duty_cycle_stop'])
    increment = float(json_object['increment'])
    pause = float(json_object['pause'])
    return StepMotorParameters(frequency, duty_cycle_start, duty_cycle_stop,
                               increment, pause)


def json_object_to_step_motor_controller(json_object):
    """Convert dictionary to step motor controller"""
    name = json_object['name']
    output = json_object['output']
    output_pin_mapping = create_output_pin_mapping(True)
    parameters = json_object_to_step_motor_parameters(json_object['parameters'])
    identifier = json_object['identifier']
    return StepMotorController(name, output, output_pin_mapping, parameters, identifier)


def json_dict_to_step_motor_controller_dict(json_dict):
    """Convert json dict to step motor controller dict"""
    return dict((key, json_object_to_step_motor_controller(json_object))
                for key, json_object in json_dict.items())
