"""Choreography related json conversions"""
from control.model import Choreography, ChoreographyPuppetEvent, ChoreographyPauseEvent


def json_array_to_choreographies(json_array):
    """Convert json array to choreographies"""
    return list(map(json_object_to_choreography, json_array))


def json_object_to_choreography(json_object):
    """Convert dictionary to choreography"""
    name = json_object['name']
    events = map(json_object_to_choreography_event, json_object['events'])
    if "identifier" in json_object:
        identifier = json_object["identifier"]
        return Choreography(name, list(events), identifier)
    else:
        return Choreography(name, list(events))


def json_object_to_choreography_event(json_object):
    """Convert dictionary from json to choreography event"""
    if json_object["type"] == "Puppet":
        identifier = int(json_object["identifier"])
        name = json_object["name"]
        return ChoreographyPuppetEvent(name, identifier)
    elif json_object["type"] == "Pause":
        length = float(json_object["length"])
        return ChoreographyPauseEvent(length)


def json_dict_to_choreography_dict(json_dict):
    """Convert json dict to choreography dict"""
    return \
        dict((str(key), json_object_to_choreography(json_object)) for key, json_object
             in json_dict.items())
