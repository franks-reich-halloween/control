"""Puppet related json conversions"""
from control.model import Puppet, create_output_pin_mapping


def json_object_to_puppet(json_object):
    """Convert dictionary to puppet"""
    output_pin_mapping = create_output_pin_mapping()
    name = json_object['name']
    output = int(json_object['output'])
    if 'identifier' in json_object:
        identifier = int(json_object['identifier'])
        return Puppet(name, output, output_pin_mapping, identifier)
    else:
        return Puppet(name, output, output_pin_mapping)


def json_array_to_puppets(json_array):
    """Convert json array to puppets"""
    return list(map(json_object_to_puppet, json_array))


def json_dict_to_puppets_dict(json_dict):
    """Convert json dict to puppets dict"""
    return \
        dict((key, json_object_to_puppet(json_object)) for key, json_object
             in json_dict.items())
