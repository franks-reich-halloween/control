"""Process and tools to save changes to the data to the file system."""
from enum import Enum
import simplejson as json
import logging
import signal
import multiprocessing as mp

from control.json_conversion import \
    json_dict_to_choreography_dict, json_dict_to_puppets_dict, \
    json_dict_to_step_motor_controller_dict


class Command(Enum):
    STORE_DATA = 1
    SHUTDOWN = 2


class ControlMessage(object):
    def __init__(self, command, data=None, data_type=""):
        if data is None:
            data = {}
        self.command = command
        self.data = data
        self.data_type = data_type


filename_mapping = {
    'Choreographies': './data/choreographies.json',
    'Puppets': './data/puppets.json',
    'ImageMap': './data/image_map.json',
    'StepMotorControllers': './data/step_motor_controllers.json'
}


def load_image_map_if_exists():
    filename = filename_mapping['ImageMap']
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
            return data
    except IOError:
        return {}


def load_choreographies_if_exist():
    filename = filename_mapping['Choreographies']
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
            return json_dict_to_choreography_dict(data)
    except IOError:
        return {}


def load_puppets_file_if_exists():
    filename = filename_mapping['Puppets']
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
            return json_dict_to_puppets_dict(data)
    except IOError:
        return {}


def load_step_motor_controller_file_if_exists():
    filename = filename_mapping['StepMotorControllers']
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
            return json_dict_to_step_motor_controller_dict(data)
    except IOError:
        return {}


def _storage_process(message_queue):
    """Retrieves storage request from the message queue and save changes to the filesystem."""
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    logging.basicConfig(level=logging.INFO)
    running = True
    while running:
        message = message_queue.get()
        if message.command == Command.SHUTDOWN:
            logging.info('Shutting down storage process.')
            running = False
        elif message.command == Command.STORE_DATA:
            _store_data(message)


def _store_data(message):
    logging.info("Handling store command")
    if message.data_type in filename_mapping:
        filename = filename_mapping[message.data_type]
        logging.info("Writing file: " + filename)
        json_string = json.dumps(message.data)
        with open(filename, 'w') as file:
            file.write(json_string)
    else:
        logging.warning("No filename registered for type" + str(message.command))


class StorageClient(object):
    """Client to communicate with the Storage process"""

    def __init__(self, storage_queue):
        self.storage_queue = storage_queue

    def store_image_map(self, data):
        logging.info("Scheduling update for image map")
        self._store(data, 'ImageMap')

    def store_puppets(self, data):
        logging.info("Scheduling update for puppets")
        self._store(data, "Puppets")

    def store_choreographies(self, data):
        logging.info("Scheduling update for choreography")
        self._store(data, "Choreographies")

    def store_step_motor_controller(self, data):
        logging.info("Scheduling update for step motor controller")
        self._store(data, "StepMotorControllers")

    def _store(self, data, data_type):
        """Schedule file to be written"""
        self.storage_queue.put(ControlMessage(Command.STORE_DATA, data, data_type))


class Storage(object):
    """Process to handle file storage"""

    def __init__(self):
        self.manager = mp.Manager()
        self.storage_queue = self.manager.Queue()
        self.process = None
        self.running = False
        self._create_process()

    def start(self):
        """Starts the process for file writing"""
        self.process.start()

    def _create_process(self):
        self.process = mp.Process(target=_storage_process, args=(self.storage_queue,))

    def stop(self):
        """Stop the process"""
        self.storage_queue.put(ControlMessage(Command.SHUTDOWN))
        self.process.join()

    def client(self):
        """Returns the client object for the process"""
        return StorageClient(self.storage_queue)
