"""Implements a service to control halloween puppets with a raspberry pi."""
import falcon


from .hal import HardwareScheduler
from .model import Puppet, PuppetCollection, ChoreographyCollection, \
                   create_output_pin_mapping, PuppetImageCollection, setup_output_pins, \
                   StepMotorController, StepMotorParameters, StepMotorControllerCollection
from .service import PuppetCollectionService, PuppetService, PuppetFunctionService, \
                     PuppetImageService, ChoreographyCollectionService, ChoreographyService, \
                     ChoreographyFunctionService, OutputPinCollectionService, \
                     FreeOutputPinCollectionService, StepMotorControllerCollectionService, \
                     StepMotorControllerService, StepMotorControllerFunctionService
from .file_access import Storage, load_choreographies_if_exist, \
                         load_puppets_file_if_exists, load_image_map_if_exists, \
                         load_step_motor_controller_file_if_exists


def wsgi_app(storage, hardware_scheduler):
    """Create the falcon app."""
    return _app(storage, hardware_scheduler)


def _app(storage, hardware_scheduler):
    setup_output_pins(hardware_scheduler)
    puppets = _create_puppets()
    choreographies = load_choreographies_if_exist()
    choreography_collection = ChoreographyCollection(
        choreographies, storage, hardware_scheduler)
    puppet_collection = _create_puppet_collection(puppets, choreography_collection, storage)
    puppet_service = PuppetService(puppet_collection)
    puppet_collection_service = PuppetCollectionService(puppet_collection)
    puppet_function_service = PuppetFunctionService(puppet_collection, hardware_scheduler)
    puppet_image_collection = _create_puppet_image_collection(storage)
    puppet_image_service = PuppetImageService(puppet_image_collection)
    choreography_collection_service = ChoreographyCollectionService(choreography_collection)
    choreography_service = ChoreographyService(choreography_collection)
    choreography_function_service = ChoreographyFunctionService(
        puppet_collection, choreography_collection, hardware_scheduler)
    output_pin_collection_service = OutputPinCollectionService()
    free_output_pin_collection_service = FreeOutputPinCollectionService(puppet_collection)
    step_motor_controller_collection = _create_step_motor_controller_collection(
        storage, hardware_scheduler)
    step_motor_controller_collection_service = StepMotorControllerCollectionService(
        step_motor_controller_collection)
    step_motor_controller_service = StepMotorControllerService(
        step_motor_controller_collection)
    step_motor_controller_function_service = StepMotorControllerFunctionService(
        step_motor_controller_collection, hardware_scheduler)
    application = falcon.API()
    application.add_route('/puppet/{puppet_identifier}', puppet_service)
    application.add_route(
        '/puppet/{puppet_identifier}/{puppet_function}', puppet_function_service)
    application.add_route('/puppet', puppet_collection_service)
    application.add_route('/puppet/image/{puppet_identifier}', puppet_image_service)
    application.add_route('/choreography', choreography_collection_service)
    application.add_route('/choreography/{choreography_identifier}', choreography_service)
    application.add_route(
        '/choreography/{choreography_identifier}/{choreography_function}',
        choreography_function_service)
    application.add_route('/output_pin', output_pin_collection_service)
    application.add_route('/free_output_pin', free_output_pin_collection_service)
    application.add_route(
        '/step_motor_controller',
        step_motor_controller_collection_service)
    application.add_route(
        '/step_motor_controller/{step_motor_controller_identifier}',
        step_motor_controller_service)
    application.add_route(
        '/step_motor_controller/{step_motor_controller_identifier}/{step_motor_controller_function}',
        step_motor_controller_function_service)
    return application


def _create_puppets():
    puppets = load_puppets_file_if_exists()
    return puppets


def _create_puppet_collection(puppets, choreographies, storage):
    collection = PuppetCollection(puppets, choreographies, storage)
    return collection


def _create_puppet_image_collection(storage):
    puppet_images = load_image_map_if_exists()
    return PuppetImageCollection(puppet_images, storage)


def _create_step_motor_controller_collection(storage, hardware_scheduler):
    step_motor_controllers = load_step_motor_controller_file_if_exists()
    if step_motor_controllers == {}:
        step_motor_controllers = {
            '0': StepMotorController(
                "bats", 11, create_output_pin_mapping(True),
                StepMotorParameters(50, 3, 10, 0.1, 0.1), "0")
        }
    return StepMotorControllerCollection(
        step_motor_controllers, storage, hardware_scheduler, create_output_pin_mapping(True))
