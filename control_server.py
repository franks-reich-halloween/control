import control
import signal
import logging
import multiprocessing as mp
import waitress


processes = {}


def _wsgi_process(storage_queue, hardware_scheduler):
    logging.basicConfig(level=logging.INFO)
    waitress.serve(
        control.wsgi_app(storage_queue, hardware_scheduler),
        listen='*:8000', threads=1)


def _create_processes():
    hardware_scheduler = control.HardwareScheduler()
    hardware_scheduler_client = hardware_scheduler.client()
    storage_process = control.Storage()
    storage_process_client = storage_process.client()
    return {
        'hardware_scheduler': hardware_scheduler,
        'storage_process': storage_process,
        'waitress': mp.Process(
            target=_wsgi_process, args=(storage_process_client, hardware_scheduler_client))
    }


def _start_processes():
    processes['hardware_scheduler'].start()
    processes['storage_process'].start()
    processes['waitress'].start()


def _shutdown_processes():
    processes['hardware_scheduler'].stop()
    processes['storage_process'].stop()


def _join_processes():
    processes['waitress'].join()


def _sigint_handler(signal, frame):
    logging.info("Shutting down server")
    _shutdown_processes()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, _sigint_handler)
    logging.basicConfig(level=logging.INFO)
    logging.info("Creating server processes")
    processes = _create_processes()
    _start_processes()
    _join_processes()
