Control Halloween Toys with a Raspberry Pi
==========================================

The server is not thread safe and is started with only one waitress thread.
This saves resources on the Raspberry PI.

## Startup

python.exe .\control_server.py

The script creates the waitress server, the scheduler process and the storage process.

## Circuit

The circuit used to trigger the halloween puppets can be found here:

https://workspace.circuitmaker.com/Projects/Details/Frank-Krick/Halloween-Trigger-Box
